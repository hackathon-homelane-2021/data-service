const csv = require("fast-csv");
const fs = require("fs");
const path = require("path");

const TABLE_NAME = "tests";

const formatter = (row) => {
  const total_samples = Number(row.TotalSamples);
  const negative = Number(row.Negative);
  const positive = Number(row.Positive);

  return {
    date: row.Date,
    state: row.State,
    total_samples: Number.isNaN(total_samples) ? undefined : total_samples,
    negative: Number.isNaN(negative) ? undefined : negative,
    positive: Number.isNaN(positive) ? undefined : positive,
  };
};

const data = [];

fs.createReadStream(
  path.resolve(__dirname, "../assets", "StatewiseTestingDetails.csv")
)
  .pipe(csv.parse({ headers: true }))
  .on("error", (error) => console.error(error))
  .on("data", (row) => {
    data.push(formatter(row));
  })
  .on("end", (rowCount) => console.log(`Parsed ${rowCount} rows`));

exports.seed = async function (knex) {
  console.log("seed started");
  // Deletes ALL existing entries
  await knex(TABLE_NAME).del();

  return knex(TABLE_NAME).insert(data); //TODO: review why not all rows processed

  // return fs
  //   .createReadStream(
  //     path.resolve(__dirname, "../assets", "covid_19_india.csv")
  //   )
  //   .pipe(csv.parse({ headers: true }))
  //   .on("error", (error) => console.error(error))
  //   .on("data", (row) => {
  //     console.log(row);
  //     // await knex(TABLE_NAME).insert(formatter(row)); // TODO: insert in chunks/bulk
  //   })
  //   .on("end", (rowCount) => console.log(`Parsed ${rowCount} rows`));
};
