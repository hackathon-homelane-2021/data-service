const csv = require("fast-csv");
const fs = require("fs");
const path = require("path");

const TABLE_NAME = "cases";

const formatter = (row) => {
  const confirmedIndians = Number(row.ConfirmedIndianNational);
  const confirmedForeigners = Number(row.ConfirmedForeignNational);
  const cured = Number(row.Cured);
  const deaths = Number(row.Deaths);
  const confirmed = Number(row.Confirmed);

  return {
    date: row.Date,
    time: row.Time,
    state: row["State/UnionTerritory"],
    confirmed_indian_national: Number.isNaN(confirmedIndians)
      ? undefined
      : confirmedIndians,
    confirmed_foreign_national: Number.isNaN(confirmedForeigners)
      ? undefined
      : confirmedForeigners,
    cured: Number.isNaN(cured) ? undefined : cured,
    deaths: Number.isNaN(deaths) ? undefined : deaths,
    confirmed: Number.isNaN(confirmed) ? undefined : confirmed,
  };
};

const data = [];

fs.createReadStream(path.resolve(__dirname, "../assets", "covid_19_india.csv"))
  .pipe(csv.parse({ headers: true }))
  .on("error", (error) => console.error(error))
  .on("data", (row) => {
    data.push(formatter(row));
  })
  .on("end", (rowCount) => console.log(`Parsed ${rowCount} rows`));

exports.seed = async function (knex) {
  console.log("seed started");
  // Deletes ALL existing entries
  await knex(TABLE_NAME).del();

  return knex(TABLE_NAME).insert(data); //TODO: review why not all rows processed

  // return fs
  //   .createReadStream(
  //     path.resolve(__dirname, "../assets", "covid_19_india.csv")
  //   )
  //   .pipe(csv.parse({ headers: true }))
  //   .on("error", (error) => console.error(error))
  //   .on("data", (row) => {
  //     console.log(row);
  //     // await knex(TABLE_NAME).insert(formatter(row)); // TODO: insert in chunks/bulk
  //   })
  //   .on("end", (rowCount) => console.log(`Parsed ${rowCount} rows`));
};
