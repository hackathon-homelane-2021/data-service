const csv = require("fast-csv");
const fs = require("fs");
const path = require("path");
const moment = require("moment");

const TABLE_NAME = "vaccines";

const formatter = (row) => {
  const total_doses_administered =
    Number(row["Total Doses Administered"]) || undefined;
  const total_sessions_conducted =
    Number(row["Total Sessions Conducted"]) || undefined;
  const total_sites = Number(row["Total Sites "]) || undefined;
  const firs_dose = Number(row["First Dose Administered"]) || undefined;
  const second_dose = Number(row["Second Dose Administered"]) || undefined;
  const male_vaccinated =
    Number(row["Male(Individuals Vaccinated)"]) || undefined;
  const female_vaccinated =
    Number(row["Female(Individuals Vaccinated)"]) || undefined;
  const transgender_vaccinated =
    Number(row["Transgender(Individuals Vaccinated)"]) || undefined;
  const total_covaxin = Number(row["Total Covaxin Administered"]) || undefined;
  const total_covishield =
    Number(row["Total CoviShield Administered"]) || undefined;
  const total_sputnik =
    Number(row["Total Sputnik V Administered"]) || undefined;
  const aefi = Number(row["AEFI"]) || undefined;
  const age_18 = Number(row["18-45 years (Age)"]) || undefined;
  const age_45 = Number(row["45-60 years (Age)"]) || undefined;
  const age_60 = Number(row["T60+ years (Age)"]) || undefined;
  const total_individuals =
    Number(row["Total Individuals Vaccinated"]) || undefined;

  return {
    date: moment(row["Updated On"], "DD/MM/YYYY"),
    state: row.State,
    total_doses_administered,
    total_sessions_conducted,
    total_sites,
    firs_dose,
    second_dose,
    male_vaccinated,
    female_vaccinated,
    transgender_vaccinated,
    total_covaxin,
    total_covishield,
    total_sputnik,
    aefi,
    age_18,
    age_45,
    age_60,
    total_individuals,
  };
};

const data = [];

fs.createReadStream(
  path.resolve(__dirname, "../assets", "covid_vaccine_statewise.csv")
)
  .pipe(csv.parse({ headers: true }))
  .on("error", (error) => console.error(error))
  .on("data", (row) => {
    data.push(formatter(row));
  })
  .on("end", (rowCount) => console.log(`Parsed ${rowCount} rows`));

exports.seed = async function (knex) {
  console.log("seed started");
  // Deletes ALL existing entries
  await knex(TABLE_NAME).del();

  return knex(TABLE_NAME).insert(data); //TODO: review why not all rows processed

  // return fs
  //   .createReadStream(
  //     path.resolve(__dirname, "../assets", "covid_19_india.csv")
  //   )
  //   .pipe(csv.parse({ headers: true }))
  //   .on("error", (error) => console.error(error))
  //   .on("data", (row) => {
  //     console.log(row);
  //     // await knex(TABLE_NAME).insert(formatter(row)); // TODO: insert in chunks/bulk
  //   })
  //   .on("end", (rowCount) => console.log(`Parsed ${rowCount} rows`));
};
