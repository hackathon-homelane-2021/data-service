## Data Service

---
### Prerequisites
* Create covid19 database
    > CREATE DATABASE covid19;

* Update `knexfile.js` with db credentials;

### Instructions

    cd /data-service
    npm install


**seed database with csv data**

Place csv files found under `/assets`.
Database schema and migration scripts are defined at `/migrations`.
    
    knex migrate:latest
    knex seed:run

**run**
  
    npm start
>"Server listening at http://127.0.0.1:4444"


---
### API Spec. 
Refer [requests.http](requests.http) for examples

Get report

> GET http://localhost:4444/report
> ?state=karnataka
> &date=2020/07/03

