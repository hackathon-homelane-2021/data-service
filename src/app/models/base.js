const logger = require("../logger");

class Base {
  connection;
  collection = "Base";

  constructor(connection, collection) {
    this.connection = connection;
    this.collection = collection;
  }

  async getById(id) {
    const whereBuilder = (builder) => {
      builder.where("id", id);
    };
    return this.connection
      .select("*")
      .from(this.collection)
      .where(whereBuilder);
  }

  static whereBuilder = (builder, filter) => {
    for (let key in filter) {
      if (Object.prototype.hasOwnProperty.call(filter, key)) {
        if (Array.isArray(filter[key])) {
          builder.whereIn(key, filter[key]);
        } else {
          builder.where(key, filter[key]);
        }
      }
    }
  };

  find(filter) {
    return this.connection
      .select("*")
      .from(this.collection)
      .where((b) => Base.whereBuilder(b, filter));
  }

  findWithinPeriod({ filter, from, to }) {
    return this.connection
      .select("*")
      .from(this.collection)
      .where((builder) => Base.whereBuilder(builder, filter))
      .whereBetween("date", [from, to])
      .orderBy("date", "asc");
  }

  save(record) {
    logger.debug({ record }, "Saving record to db.");
    return this.connection
      .insert(record, "*")
      .into(this.collection)
      .onConflict()
      .ignore();
  }

  saveMany(records) {
    logger.debug({ records }, "Saving records to db.");
    return this.connection
      .insert(records, "*")
      .into(this.collection)
      .onConflict()
      .ignore();
  }
}

module.exports = Base;
