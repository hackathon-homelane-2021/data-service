const Base = require("./base");

class TestDetail extends Base {
  constructor(connection) {
    super(connection, "tests");
  }
}

module.exports = TestDetail;
