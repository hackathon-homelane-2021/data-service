const Base = require("./base");

class Case extends Base {
  constructor(connection) {
    super(connection, "cases");
  }
}

module.exports = Case;
