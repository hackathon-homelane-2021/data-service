const Case = require("./case");
const Test = require("./covid-test");
const Vaccine = require("./vaccine");

module.exports = (connection) => {
  return {
    caseRepo: new Case(connection),
    testRepo: new Test(connection),
    vaccineRepo: new Vaccine(connection),
  };
};
