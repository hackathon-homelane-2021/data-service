const Base = require("./base");

class Vaccination extends Base {
  constructor(connection) {
    super(connection, "vaccines");
  }
}

module.exports = Vaccination;
