module.exports = (fastify) => {
  const {
    repository: { caseRepo, testRepo, vaccineRepo },
  } = fastify;

  /**
   * @param {Object} query filter object
   * @param {Date} query.date filter result by given date.
   * @param {Array|String} query.state filter result by states */
  const getAll = async (query) => {
    let report = {
      cases: [],
      tests: [],
      vaccines: [],
    };
    report.cases = await caseRepo.find(query);
    report.tests = await testRepo.find(query);
    report.vaccines = await vaccineRepo.find(query);

    return report;
  };

  // TODO implement paginated join method if necessary

  return {
    getAll,
  };
};
