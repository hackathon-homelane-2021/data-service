const service = jest.genMockFromModule("../service.js");

module.exports = () => {
  service.getAll = jest.fn();

  return service;
};
