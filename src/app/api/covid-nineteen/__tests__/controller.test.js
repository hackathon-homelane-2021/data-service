const Controller = require("../controller");
const Service = require("../service");
jest.mock("../service");

describe("Controller Test", () => {
  let controller;
  let request;
  let response;
  let service = Service();

  beforeAll(() => {
    controller = Controller({});
  });

  describe("getAll", () => {
    beforeEach(async () => {
      request = {
        query: {
          state: "state",
        },
      };
      service.getAll.mockResolvedValueOnce("done");
      response = await controller.getAll(request);
    });

    it("should call service.getAll", function () {
      expect(service.getAll).toHaveBeenLastCalledWith(request.query);
    });
    it("should return response", function () {
      expect(response).toEqual("done");
    });
  });
});
