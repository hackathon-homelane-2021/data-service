const Service = require("../service");

const repo = {
  find: jest.fn().mockResolvedValue(["row"]),
};

describe("Service Test", () => {
  let service;
  let request;
  let response;

  beforeAll(() => {
    service = Service({
      repository: {
        caseRepo: repo,
        testRepo: repo,
        vaccineRepo: repo,
      },
    });
  });

  describe("getAll", () => {
    beforeEach(async () => {
      request = {
        state: "state",
      };
      response = await service.getAll(request);
    });

    it("should call repo.find", function () {
      expect(repo.find).toHaveBeenCalledWith(request);
    });
    it("should call find thrice for each table", function () {
      expect(repo.find).toHaveBeenCalledTimes(3);
    });
    it("should return response", function () {
      expect(response).toEqual({
        cases: ["row"],
        tests: ["row"],
        vaccines: ["row"],
      });
    });
  });
});
