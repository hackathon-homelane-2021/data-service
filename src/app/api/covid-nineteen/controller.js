const Service = require("./service");

module.exports = (fastify) => {
  const service = Service(fastify);

  const getAll = async (req) => {
    return service.getAll(req.query);
  };

  return {
    getAll,
  };
};
