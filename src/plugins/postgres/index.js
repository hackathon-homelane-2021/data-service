const path = require("path");

const fp = require("fastify-plugin");
const knex = require("knex");
const knexStringcase = require("knex-stringcase");
const { attachPaginate } = require("knex-paginate");
const Repo = require("../../app/models");
attachPaginate();

const defaultDbConfig = {
  client: "postgres",
  pool: { min: 1, max: 10 },
  acquireConnectionTimeout: 10000,
  migrations: {
    tableName: "knex_migrations",
    directory: path.resolve(__dirname, "../../../migrations"),
  },
};

const connectionCheck = (client) => client.raw("select 1+1 as result");

async function knexPlugin(fastify, options) {
  try {
    const client = knex(
      knexStringcase({
        ...defaultDbConfig,
        connection: {
          host: fastify.config.DB_HOST,
          user: fastify.config.DB_USER,
          password: fastify.config.DB_PASSWORD,
          database: fastify.config.DB_NAME,
          port: fastify.config.DB_PORT,
        },
        asyncStackTraces: true,
        debug: true,
        ...options,
      })
    );
    fastify.decorate("repository", Repo(client));
    fastify.decorate("knex", client);
    await connectionCheck(client);
    fastify.log.info("Knexen to db registered successfully");
    fastify.addHook("onClose", (instance, done) => {
      client.destroy().then(done).catch(done);
    });
  } catch (e) {
    fastify.log.error(`DB connection failed: ${process.env.NODE_ENV}`);
    throw Error("Connection Failed " + e);
  }
}

module.exports = fp(knexPlugin);
