const fastifyEnv = require("fastify-env");
const underPressure = require("under-pressure");

module.exports = async function (fastify, opts) {
  fastify.register(fastifyEnv, {
    schema: {
      type: "object",
      properties: {
        MAX_EVENT_LOOP_DELAY: {
          type: "integer",
          default: 1000,
        },
        MAX_HEAP_USED_BYTES: {
          type: "integer",
          default: 0,
        },
        MAX_RSS_BYTES: {
          type: "integer",
          default: 0,
        },
      },
    },
    data: opts,
  });

  fastify.register(async function (fastify) {
    fastify.register(underPressure, {
      maxEventLoopDelay: fastify.config.MAX_EVENT_LOOP_DELAY,
      maxHeapUsedBytes: fastify.config.MAX_HEAP_USED_BYTES,
      maxRssBytes: fastify.config.MAX_RSS_BYTES,
      exposeStatusRoute: "/live",
    });

    fastify.get("/ready", async (request, reply) => {
      reply.code(fastify.serviceAvailable ? 200 : 503).send();
    });
  });

  ["SIGTERM", "SIGINT"].forEach((signal) => {
    process.on(signal, () => {
      fastify.log.info(`${signal} signal received. Terminating service`);
      fastify.serviceAvailable = false;
      fastify.close(() => {
        process.exit(0);
      });
    });
  });
};
