const fp = require("fastify-plugin");
const statusCodes = require("http").STATUS_CODES;
const VALIDATION_ERROR = "001";

module.exports = fp((fastify, opts, next) => {
  fastify.setErrorHandler(async (error, request, reply) => {
    request.log.error(error);

    const statusCode =
      Array.isArray(error) && error.length
        ? error[0].status
        : error.status ||
          error.statusCode ||
          (error.errors && error.errors.length && error.errors[0].status) ||
          reply.statusCode ||
          reply.raw.statusCode ||
          "500";

    if (error.validation) {
      reply.code(400).send({
        errors: [
          {
            code: VALIDATION_ERROR,
            message: "Validation error",
            detail: error.message,
            status: "400",
          },
        ],
      });
    }

    let errors;

    if (Array.isArray(error)) {
      errors = error.map((err) => ({
        id: err.id,
        code: err.code || statusCodes[statusCode + ""],
        status: err.status.toString(),
        message: err.message,
        detail: err.detail,
        meta: err.meta,
      }));
    } else {
      // API ERRORS
      if (error.errors) {
        errors = error.errors;
      } else {
        errors = [
          {
            id: error.id,
            code: error.code || statusCodes[statusCode + ""],
            status: statusCode.toString(),
            message: error.message,
            detail: error.detail,
            meta: error.meta,
          },
        ];
      }
    }
    reply.code(statusCode).send({ errors });
  });
  next();
});
