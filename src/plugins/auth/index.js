const fp = require("fastify-plugin");
const jwt = require("jsonwebtoken");

module.exports = fp(async (fastify, opts, next) => {
  fastify.decorateRequest("auth", {
    isAuthenticated: false,
  });

  const { JWT_SECRET } = fastify.config;

  fastify.addHook("preHandler", (req, res, done) => {
    const accessToken = req.headers["access-token"];

    if (accessToken && accessToken.length) {
      return jwt.verify(accessToken, JWT_SECRET, {}, (err, decoded) => {
        if (err) {
          fastify.log.error({ err }, "Error decoding token");
          res.code(401);
          done(new Error("Invalid Token"));
        } else {
          req.auth = {
            isAuthenticated: true,
            token: accessToken,
            clientId: decoded.clientId,
          };
          done();
        }
      });
    } else {
      res.code(403);
      done(new Error("Not authorised"));
    }

    next();
  });
});
