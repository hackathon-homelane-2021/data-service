exports.up = function (knex) {
  return knex.schema.createTable("cases", function (table) {
    table.increments();

    table.date("date").notNullable();
    table.time("time").notNullable();
    table.string("state").notNullable();
    table.integer("confirmed_indian_national");
    table.integer("confirmed_foreign_national");
    table.integer("cured");
    table.integer("deaths");
    table.integer("confirmed");

    table.timestamp("created_at").notNullable().defaultTo(knex.fn.now());

    table.unique(["date", "time", "state"]);
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("cases");
};
