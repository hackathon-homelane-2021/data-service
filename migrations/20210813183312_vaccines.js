exports.up = function (knex) {
  return knex.schema.createTable("vaccines", function (table) {
    table.increments();

    table.date("date").notNullable();
    table.string("state");
    table.integer("total_doses_administered");
    table.integer("total_sessions_conducted");
    table.integer("total_sites");
    table.integer("firs_dose");
    table.integer("second_dose");
    table.integer("male_vaccinated");
    table.integer("female_vaccinated");
    table.integer("transgender_vaccinated");
    table.integer("total_covaxin");
    table.integer("total_covishield");
    table.integer("total_sputnik");
    table.integer("aefi");
    table.integer("age_18");
    table.integer("age_45");
    table.integer("age_60");
    table.integer("total_individuals");

    table.unique(["date", "state"]);
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("vaccines");
};
