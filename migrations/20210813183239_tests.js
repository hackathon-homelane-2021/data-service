exports.up = function (knex) {
  return knex.schema.createTable("tests", function (table) {
    table.increments();

    table.date("date").notNullable();
    table.string("state");
    table.integer("total_samples");
    table.integer("negative");
    table.integer("positive");

    table.timestamp("created_at").notNullable().defaultTo(knex.fn.now());

    table.unique(["date", "state"]);
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tests");
};
